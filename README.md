# manual-jobs

Ein Beispiel Projekt für die Verwendung von manuellen Schritten in GitLab-CI Pipelines.

Weitere Informationen zu [when:manual](https://docs.gitlab.com/ee/ci/yaml/#whenmanual)
When gleichzeitig Protected Environments benutzt werden kann hiermit auch ein QA-Gating realisiert werden. Siehe [`Protecting manual jobs`](https://docs.gitlab.com/ee/ci/yaml/#protecting-manual-jobs-premium)
